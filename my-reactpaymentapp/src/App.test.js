import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders List Payments link', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/List Payments/i);
  expect(linkElement).toBeInTheDocument();
});
