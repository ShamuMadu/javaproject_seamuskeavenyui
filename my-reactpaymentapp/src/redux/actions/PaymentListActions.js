import {PAYMENT_LIST_REQUEST , PAYMENT_LIST_SUCCESS , PAYMENT_LIST_FAIL}
    from "../constants/PaymentConstants"

import PaymentRestService from "../../services/PaymentRestService";

const paymentList = () => (dispatch) => {
    try {
//        console.log(">>> Payment List in dispatch ");
        dispatch({ type: PAYMENT_LIST_REQUEST });
        PaymentRestService.getAll().then(response => {
//            console.log(">>> SUCCESSFUL Payment List - data: " + response.data);
            dispatch({ type: PAYMENT_LIST_SUCCESS, payload: response.data });
        });
    } catch (error) {
//        console.log(">>> FAILED Payment List - error: " + error);
        dispatch({ type: PAYMENT_LIST_FAIL, payload: error });
    }
};

export default paymentList;