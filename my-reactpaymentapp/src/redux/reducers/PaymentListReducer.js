import {PAYMENT_LIST_REQUEST , PAYMENT_LIST_SUCCESS , PAYMENT_LIST_FAIL}
    from "../constants/PaymentConstants"

//Payload is what is bundled in your actions and passed around between reducers in your redux application.
// //For example - const someAction = { type: "Test", payload: {user: "Test User", age: 25}, }
const paymentsInitialState = {
    pending: false,
    payments: [],
    error: null
}

function PaymentListReducer(state=paymentsInitialState, action)
{
//    console.log("Reducer = " + action.type);

    switch (action.type) {
        case PAYMENT_LIST_REQUEST:
//            console.log(">>> Payment List Reducer REQUEST");
            return { ...state, pending: true };
        case PAYMENT_LIST_SUCCESS:
//            console.log(">>> Payment List Reducer SUCCESS");
            return { ...state, pending: false, payments: action.payload };
        case PAYMENT_LIST_FAIL:
//            console.log(">>> Payment List Reducer FAIL");
            return { ...state, pending: false, error: action.payload };

        default:
            return state;
    }
}

export default PaymentListReducer;