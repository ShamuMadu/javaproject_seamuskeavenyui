import {applyMiddleware, createStore} from "redux";
import PaymentListReducer from "./reducers/PaymentListReducer"
import thunk from "redux-thunk";

const initialState = {};
export const middlewares = [thunk];
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ //|| compose;

// TODO: Need to look at why referencing the combined "reducer" doesn`t appear to work.
export const store = createStore(
    PaymentListReducer,
    initialState,
    composeEnhancer(applyMiddleware(...middlewares))
);
