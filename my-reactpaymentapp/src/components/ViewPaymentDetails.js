import React, {Component} from "react";
import {connect} from "react-redux";
import paymentList from "../redux/actions/PaymentListActions";

class ViewPaymentDetails extends Component {
    constructor(props) {
        super(props);
        this.props.dispatch(paymentList());
        this.returnToList = this.returnToList.bind(this);
    }

    returnToList() {
        this.props.history.push("/list");
    }

    render() {
        const matchingId = this.props.match.params.id;
        const {payments} = this.props;
//        console.log("VIEW PAYMENT DETAILS - RENDER: " + this.props.payments);
        const payment = ( payments && payments.find(item => item.id == matchingId));

        if (!(payment)) {
            return (
                <>
                    <h1>View Payment Details</h1>
                    Unable to find payment
                </>
            );
        }
        return (
            <>
                <h1>View Payment Details</h1>
                <div className="form-group row">
                    <label htmlFor="paymentDate" className="col-sm-2 col-form-label">Payment Date</label>
                    <div className="col-sm-10">
                    <label className="form-control-plaintext" id="paymentDate">
                        {new Intl.DateTimeFormat("en-GB", {
                            year: "numeric",
                            month: "long",
                            day: "2-digit"
                        }).format(new Date(payment.paymentDate))}
                    </label>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="type" className="col-sm-2 col-form-label">Type</label>
                    <div className="col-sm-10">
                        <label className="form-control-plaintext" id="type">
                            {payment.type}
                        </label>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="amount" className="col-sm-2 col-form-label">Amount</label>
                    <div className="col-sm-10">
                        <label className="form-control-plaintext" id="amount">
                            {new Intl.NumberFormat("en-GB", {
                                style: "currency",
                                currency: "GBP"
                            }).format(payment.amount)}
                        </label>
                    </div>
                </div>
                <div className="form-group">
                    <input type="button" className="form-control btn btn-primary" value="Return to List Payments" onClick={this.returnToList}/>
                </div>
            </>
        );
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: () => dispatch(paymentList())
    };
};

const mapStateToProps = state => ({
    payments: state.payments
})

export default connect(mapStateToProps,mapDispatchToProps)(ViewPaymentDetails);