import React, {Component} from "react";
import {connect} from "react-redux";
import paymentList from "../redux/actions/PaymentListActions";
import {Link} from "react-router-dom";

class ListPayments extends Component {
    constructor(props) {
        super(props);
//        console.log("BEFORE constructor call to dispatch: " + this.props.payments);
        this.props.dispatch();
//        console.log("AFTER constructor call to dispatch: " + this.props.payments);
    }

    render() {
        const {payments, currentIndex} = this.props;
//        console.log("LIST PAYMENTS RENDER: " + this.props.payments + " " + this.props.currentIndex);

        return  (
            <div>
                <h1>Payments List</h1>

                <ul className="list-group">
                    {payments &&
                    payments.map((payment, index) => (
                        <li
                            className={
                                "list-group-item " +
                                (index === currentIndex ? "active" : "")
                            }
                            // onClick={() => this.setActiveTutorial(tutorial, index)}
                            key={index}
                        >
                            <Link className="btn btn-link" to={{ pathname : "/view/" + payment.id}}>
                                {new Intl.DateTimeFormat("en-GB", {
                                    year: "numeric",
                                    month: "long",
                                    day: "2-digit"
                                }).format(new Date(payment.paymentDate))}
                                &nbsp;-&nbsp;
                                {payment.type}
                                &nbsp;-&nbsp;
                                {new Intl.NumberFormat("en-GB", {
                                    style: "currency",
                                    currency: "GBP"
                                }).format(payment.amount)}
                            </Link>
                        </li>
                    ))}
                </ul>
            </div>
        )
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: () => dispatch(paymentList())
    };
};

const mapStateToProps = state => ({
    error: state.error,
    payments: state.payments,
    pending: state.pending
})

export default connect(mapStateToProps,mapDispatchToProps)(ListPayments);