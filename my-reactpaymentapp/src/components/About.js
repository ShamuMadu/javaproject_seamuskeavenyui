import React, {useEffect, useState} from "react";
import axios from "axios";

function About() {
  const[apiStatus, setApiStatus] = useState("Unknown");

  useEffect(() => {
      const fetchData = async () => {
          try {
              const result = await axios(
                  "http://localhost:8080/api/payment/status"
              );
//              console.log("api status call succeeded with data: " + result.data);
              setApiStatus(result.data);
          } catch (err) {
//              console.log("api status call failed with message: " + err.message);
              setApiStatus(err.message);
          }
      };
      fetchData();
  }, []);

  return (
    <>
        <h1>About</h1>
        <h2>{apiStatus}</h2>
    </>
  );
}

export default About;