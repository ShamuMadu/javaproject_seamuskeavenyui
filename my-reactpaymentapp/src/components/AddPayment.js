import React, {Component} from "react";
import PaymentRestService from "../services/PaymentRestService";
import {Redirect} from "react-router-dom";

export default class AddPayment extends Component {
    constructor(props) {
        super(props);
        this.onChangePaymentDate = this.onChangePaymentDate.bind(this);
        this.onChangeType = this.onChangeType.bind(this);
        this.onChangeAmount = this.onChangeAmount.bind(this);
        this.onChangeId = this.onChangeId.bind(this);
        this.savePayment = this.savePayment.bind(this);
        this.returnToList = this.returnToList.bind(this);

        this.state = {
            id: 0,
            paymentDate: "",
            type: "",
            amount: 0,
            submitted: false
        };
    }

    returnToList() {
        this.props.history.push("/list");
    }

    onChangePaymentDate(e) {
        this.setState({
            paymentDate: e.target.value
        });
    }

    onChangeType(e) {
        this.setState({
            type: e.target.value
        });
    }

    onChangeId(e) {
        this.setState({
            id: e.target.value
        });
    }

    onChangeAmount(e) {
        this.setState({
            amount: e.target.value
        });
    }

    savePayment() {
        var data = {
            id: this.state.id,
            paymentDate: this.state.paymentDate,
            type: this.state.type,
            amount: this.state.amount
        }

        PaymentRestService.create(data)
            .then(response => {
                this.setState({
                    submitted: true
                });
            })
            .catch(e => {
                alert(e);
                console.log(e);
            });
    }

    render() {
        if (this.state.submitted) {
            return <Redirect to="list"/>
        }
        return (
            <form>
                <h1>Add Payment</h1>
                <div className="form-group row">
                    <label htmlFor="paymentDate" className="col-sm-2">Payment Date</label>
                    <div className="col-sm-10">
                        <input type="date" className="form-control" id="paymentDate"
                               placeholder="Enter Payment Date" value={this.state.paymentDate}
                               onChange={this.onChangePaymentDate} required/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="type" className="col-sm-2">Type</label>
                    <div className="col-sm-10">
                        <input type="text" className="form-control" id="type"
                               placeholder="Enter Type" value={this.state.type} onChange={this.onChangeType} required/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="amount" className="col-sm-2">Amount</label>
                    <div className="col-sm-10">
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <span className="input-group-text">£</span>
                            </div>
                            <input type="number" className="form-control" id="amount" placeholder="Enter Amount" value={this.state.amount} onChange={this.onChangeAmount} required/>
                        </div>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="id" className="col-sm-2">Id</label>
                    <div className="col-sm-10">
                        <input type="number" className="form-control" id="id" aria-describedby="idHelp"
                               placeholder="Enter Id" value={this.state.id} onChange={this.onChangeId} required/>
                    </div>
                </div>
                <div className="form-group">
                    <input type="button" className="form-control btn btn-primary col-sm-6" value="Save" onClick={this.savePayment}/>
                    <input type="button" className="form-control btn btn-secondary col-sm-6" value="Cancel" onClick={this.returnToList}/>
                </div>
            </form>
        );
    };
};
