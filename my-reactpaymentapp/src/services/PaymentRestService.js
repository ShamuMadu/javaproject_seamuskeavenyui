import http from "../http-common";

export class PaymentRestService {
    getAll() {
        return http.get("/payment/all");
    }

    getById(id) {
        return http.find(`/payment/find/${id}`);
    }

    getByType(type) {
        return http.find(`/payment/findbytype/${type}`);
    }

    create(data) {
        return http.post("/payment/save", data);
    }
}

export default new PaymentRestService(); // exports as an instance (new)