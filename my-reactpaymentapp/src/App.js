import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle.min";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import AddPayment from "./components/AddPayment"
import ViewPaymentDetails from "./components/ViewPaymentDetails"
import ListPayments from "./components/ListPayments"
import Home from "./components/Home"
import About from "./components/About";

function App() {
  return (
      <div id="container">

        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <a className="navbar-brand" href="/">Navbar</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                  aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item active">
                <a className="nav-link" href="/home">Home <span className="sr-only">(current)</span></a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/list">List Payments</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/add">Add Payment</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/about">About</a>
              </li>
            </ul>
          </div>
        </nav>

        <div className="container mt-3">
          <BrowserRouter>
            <Switch>
              <Route exact path={["/", "/home"]} component={Home} />
              <Route exact path="/list" component={ListPayments} />
              <Route exact path="/add" component={AddPayment} />
              <Route exact path="/about" component={About} />
              <Route path="/view/:id" component={ViewPaymentDetails} />
            </Switch>
          </BrowserRouter>
        </div>

      </div>
  );
}

export default App;
